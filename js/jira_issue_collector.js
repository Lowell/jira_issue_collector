(function ($) {
  Drupal.behaviors.jira_issue_collector = {
    attach: function (context, settings) {
      if (typeof settings.jira_issue_collector_code != 'undefined') {
        jQuery.ajax({
          url: settings.jira_issue_collector_code.url,
          type: "get",
          cache: true,
          dataType: "script"
        });
      }
    }
  };
})(jQuery);

